<?php

// Muhammad Esa Yusriana 

require_once ('./Animal.php');
require_once ('./Ape.php');
require_once ('./Frog.php');

$sheep = new Animal("shaun");
$sheep->print_all();

echo "<br>";

$kodok = new Frog("buduk");
$kodok->jump();

echo "<br>";

$sungokong = new Ape("kera sakti");
$sungokong->yell();