<?php

class Animal
{
    public function __construct($name) 
    {
        $this->name= $name;
    }
    public $legs = 4;
    public $cold_blooded = 'no';

    public function get_name()
    {
        echo "Name : " . $this->name . "<br>";
    }
    
    public function get_legs()
    {
        echo "Legs : " . $this->legs . "<br>";
    }
    
    public function get_cold_blooded()
    {
        echo "Cold Blooded : " . $this->cold_blooded . "<br>";
    }

    public function print_all()
    {
        $this->get_name();
        $this->get_legs();
        $this->get_cold_blooded();
    }
}